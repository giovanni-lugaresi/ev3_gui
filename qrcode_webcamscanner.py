from tkinter import *
from PIL import ImageTk, Image
import cv2
import numpy as np
import pyzbar.pyzbar as pyzbar

#Capture from camera
cap = cv2.VideoCapture(1)   # 0 : front camera
                            # 1 : back camera

root = Tk()
#Create Streaming Frame
stream_frame = Frame(root, bg="white")
stream_frame.grid()
#Create a label in the frame
lmain = Label(stream_frame)
lmain.grid()

#Create Buttons Frame
buttons_frame = Frame(root, bg="white")
buttons_frame.grid()

#Function for video streaming
def video_stream():
    _, frame = cap.read()
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(1, video_stream)

#Function for qr code scan
def qr_scan():

    j = 0

    while True:
        _, frame = cap.read()

        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #_, thresh = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        decodeObjects = pyzbar.decode(frame)

        i=0

        for obj in decodeObjects:   
            i = i+1 

        if i == 6:

            for obj in decodeObjects:
                print("Data: ", obj.data) 

            break

        if i == 0:

            j = j+1

            if j == 10:
                print("Nessun codice QR")
                break
    


#Create scan button
start_button = Button(buttons_frame, text="scan", bg="green", command = qr_scan)
start_button.grid()


video_stream()
root.mainloop()


if __name__ == "__main__":
    root.mainloop()